MA SSO SP
==========
Message Agency Single Sign On Service Provider.

*For the Identity Provider (IDP) version, see: https://bitbucket.org/messageagency/ma_sso_idp*

Purpose
-------
This module is meant to run some sanity-checks to help troubleshoot your SAML SSO SP setup. It's purpose is NOT to do the work of configuring your Drupal-based Simple SAML SP, but to provide some sane defaults and help walk through your install. As such, very particular assumptions are made about how your IDP and SP will be set up. Default config files are provided for reference, which you will need to customize.

Quick start for using this module
---------------------------------
- `composer require messageagency/ma_sso_sp`
- `drush en ma_sso_sp`
- go to `admin/reports/status` and work through any errors, per below.

Assumptions / Prerequisites
--------------------------
* This Drupal site will have a single, default IDP, which is another Drupal site
* This Drupal site will implement only one SP
* Installation and config will be managed by composer
* `composer.json` is in your VCS root
* Your drupal webroot is `/web`
* Your simple saml web install will be symlinked from `https://example.com/simplesaml`
* This module will be installed to `web/modules/message_agency/ma_sso_sp`
* Drupal private files directory lives at `web/sites/default/files/private`
* Everything is served over `https://`, though self-signed certs are ok.

Detailed SP setup guide
------------------------
1. **Require this module**
    ```
        composer require messageagency/ma_sso_sp
    ```

    ------

1. **Create the simplesaml symlink**

    Add a symlink in your webroot pointing to simplesaml www directory. 
    
    ```
        cd ./web
        ln -s ../vendor/simplesamlphp/simplesamlphp/www simplesaml
    ``` 
    ------

1. **Create local sso directory and copy config**
    
    Create a local sso directory, e.g. in `$DRUPAL_ROOT/sso`, and copy the config from `ma_sso_sp/sso` into it.
    
    ```
    mkdir sso 
    cp web/modules/message_agency/ma_sso_idp/sso/* sso/ 
    ``` 

1. Edit `saml20-idp-remote.php` to point to your IDP

    Replace all instances of `www.messageagency.com` with the domain of your IDP.
    
    ------

1. Edit `authsources.php` to point to your IDP    

    Replace `www.messageagency.com` with the domain of your IDP.
    
    ------

1. **Composer tasks**

    Add the following rules to composer.json, so that composer tasks don't destroy your saml config. 

    ```
    "post-install-cmd": [
        "cp sso/config.php ./vendor/simplesamlphp/simplesamlphp/config/config.php",
        "cp sso/authsources.php ./vendor/simplesamlphp/simplesamlphp/config/authsources.php",
        "cp sso/saml20-idp-remote.php ./vendor/simplesamlphp/simplesamlphp/metadata/saml20-sp-remote.php",
    ],
    "post-update-cmd": [
        "cp sso/config.php ./vendor/simplesamlphp/simplesamlphp/config/config.php",
        "cp sso/authsources.php ./vendor/simplesamlphp/simplesamlphp/config/authsources.php",
        "cp sso/saml20-idp-remote.php ./vendor/simplesamlphp/simplesamlphp/metadata/saml20-idp-hosted.php"
    ],
    ``` 
    ------

1. **Require this module (again) or reinstall to deploy configs**
    
    To deploy the configs from previous step, require this module or run `composer install`.
    ```
        composer require messageagency/ma_sso_sp
    ```

    ------

1. **Install certificates**

    ```
    openssl req -newkey rsa:3072 -new -x509 -days 3652 -nodes -out cert.pem -keyout key.pem
    mkdir -p ./web/sites/default/files/private/simplesaml/cert
    mv *.pem ./web/sites/default/files/private/simplesaml/cert
    ```
    ------

1. Check your install on your Drupal site's status report, e.g. "https://example.com/admin/reports/status"

    ------

1. Visit your simplesaml install "Authentication" tab, e.g. `https://example.com/simplesaml/module.php/core/authenticate.php`. There should be a link to `default-sp`. If following this link takes you to the drupal login page on your IDP, you're on the right track.

    ------

1. **Finally, inform your IDP of the new Service Provider.** In your IDP system, edit `saml-sp-remote.php` to include the domain of your new SP.

    ------

Additional considerations
-------------------------

#### .htaccess rules for apache
If you're running apache, you'll need to authorize access to simplesaml by adding the following in your `.htaccess` file.

Immediately prior to the line 
```  
RewriteRule "^(.+/.*|autoload)\.php($|/)" - [F]
```
add the following directives:
```
  # Allow access to simplesamlphp scripts.
  RewriteCond %{REQUEST_URI} !/simplesaml/[^/]*\.php$
  RewriteCond %{REQUEST_URI} !/simplesaml/admin/[^/]*\.php$
  RewriteCond %{REQUEST_URI} !/simplesaml/[^/]*\.php/sanitycheck/[^/]*\.php$
  RewriteCond %{REQUEST_URI} !/simplesaml/[^/]*\.php/drupalauth/[^/]*\.php$
  RewriteCond %{REQUEST_URI} !/simplesaml/[^/]*\.php/core/idp/[^/]*\.php$
  RewriteCond %{REQUEST_URI} !/simplesaml/[^/]*\.php/saml/[^/]*\.php$
  RewriteCond %{REQUEST_URI} !/simplesaml/saml2/idp/[^/]*\.php$
  RewriteCond %{REQUEST_URI} !/simplesaml/saml2/idp/[^/]*\.php/drupal-userpass$
  RewriteCond %{REQUEST_URI} !/simplesaml/[^/]*\.php/saml/sp/[^/]*\.php/default-sp$
```

#### Testing locally
In addition to editing .htaccess, you'll need to edit the values in `config.php` to point to your local database and codebase if you want to test locally.