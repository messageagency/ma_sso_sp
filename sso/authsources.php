<?php

$config = [
  'default-sp' => [
    'saml:SP',
    'idp' => 'https://www.messageagency.com/simplesaml/saml2/idp/metadata.php',
    'privatekey' => 'key.pem',
    'certificate' => 'cert.pem',
  ],
];
