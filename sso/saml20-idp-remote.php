<?php

$metadata['https://www.messageagency.com/simplesaml/saml2/idp/metadata.php'] = array(
  'metadata-set' => 'saml20-idp-remote',
  'entityid' => 'https://www.messageagency.com/simplesaml/saml2/idp/metadata.php',
  'SingleSignOnService'  => 'https://www.messageagency.com/simplesaml/saml2/idp/SSOService.php',
  'SingleLogoutService'  => 'https://www.messageagency.com/simplesaml/saml2/idp/SingleLogoutService.php',
  'certificate'          => 'cert.pem',
);
